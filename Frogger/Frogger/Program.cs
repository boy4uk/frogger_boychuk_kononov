﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Frogger
{
    class Program
    {
        static void Main(string[] args)
        {
            //Украшательства
            Console.BufferWidth = Console.WindowWidth = Map.Collomns + 1;
            Console.BufferHeight = Console.WindowHeight = Map.Rows + 1;
            Console.CursorVisible = false;

            var map = new Map();
            map.FillMap();
            map.DrawMap();

            var cars = new List<Car>();
            for (int i = 0; i < 30; i++)
                cars.Add(new Car());

            var timbers = new List<Timber>();
            for (int i = 0; i < Water.endBorder - Water.startBorder + 1; i++)
                timbers.Add(new Timber(Water.startBorder+i));


            var frog = new Frog();

            var eagle = new Eagle();

            while (!Game.gameover)
            {
                eagle.Atack();

                foreach (var e in cars)//смерть машиной
                    if (Frog.X == e.X && Frog.Y == e.Y)
                        Game.GameOver();

                if (Frog.Y >= Water.startBorder && // смерть от воды !!!!НЕ РАБОТАЕТ
                    Frog.Y <= Water.endBorder &&
                    !Frog.FrogIsRidingTimber)
                    Game.GameOver();

                if (Frog.Y == 1)
                    Game.Victory();

                if (Console.KeyAvailable)//передвижение жабки
                {
                    var keyPressed = Console.ReadKey(true).Key;
                    frog.Move(keyPressed);
                }

                foreach (var e in cars)
                    e.Move();

                foreach (var e in timbers)
                    e.Move();

                foreach (var e in timbers)//жабка едет на бревне
                    if (Frog.Y == e.Y && Frog.X >= e.X && Frog.X <= e.X + e.Length - 1)
                    {
                        frog.Move(ConsoleKey.LeftArrow);
                        Frog.FrogIsRidingTimber = true;
                    }
                    else
                    {
                        Frog.FrogIsRidingTimber = false;
                    }

                Thread.Sleep(200);
            }
        }
    }
}
