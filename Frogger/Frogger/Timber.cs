﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Frogger
{
    class Timber
    {
        public int X;
        public int Y;
        public int Length;
        public static string Icon = "0";
        private static Random rnd = new Random();
        private readonly int RightWaterBorder = Map.Collomns - 1;

        public Timber(int y)
        {
            X = rnd.Next(1, RightWaterBorder);
            Y = y;
            Length = rnd.Next(25, 35);
        }

        public void Move()
        {
            //удаление предыдущей позиции бревна
            for (int i = X; i < X + Length; i++)
                if (i > 0 && i < RightWaterBorder)
                {
                    Console.SetCursorPosition(i, Y);
                    Console.Write(Map.GameMap[Y, i]);
                }

            if (X + Length == 0)
                X = RightWaterBorder;
            else
                X--;
            Draw();
        }

        public void Draw()
        {
            for (int i = X; i < X + Length; i++)
                if (i > 0 && i < RightWaterBorder)
                {
                    Console.SetCursorPosition(i, Y);
                    Console.Write(Icon);
                }
        }
    }
}
