﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Frog
    {
        public static string Icon = "@";
        public static int X = Map.Collomns / 2;
        public static int Y = Map.Rows - 2;

        public static bool FrogIsRidingTimber = false;

        public Frog()
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(Icon);
        }


        public void Move(ConsoleKey direction)
        {
            // Стираем следы (все пиксели игрового объекта на поле)
            Console.SetCursorPosition(X, Y);
            Console.Write(Map.GameMap[Y, X]);
            switch (direction)
            {
                case ConsoleKey.LeftArrow:
                    if (X > 1)
                    {
                        X--;
                        Eagle.ReserEagleTimer(); // вообще надо бы засунуть это все в Eagle.
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (X < Map.Collomns - 2)
                    {
                        X++;
                        Eagle.ReserEagleTimer();
                    }
                    break;
                case ConsoleKey.UpArrow:
                    if (Y > 1)
                    {
                        Y--;
                        Eagle.ReserEagleTimer();
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (Y < Map.Rows - 2)
                    {
                        Y++;
                        Eagle.ReserEagleTimer();
                    }
                    break;
            }
            // Отрисовываем объект в новой позиции
            Console.SetCursorPosition(X, Y);
            Console.Write(Icon);
        }

    }
}
