﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Game
    {
        public static bool gameover = false;

        public static void GameOver()
        {
            Console.Clear();
            Console.SetCursorPosition(Map.Collomns / 2, Map.Rows / 2);
            Console.Write("GAME OVER");
            gameover = true;
            Console.ReadKey();
        }

        public static void Victory()
        {
            Console.Clear();
            Console.SetCursorPosition(Map.Collomns / 2, Map.Rows / 2);
            Console.Write("YOU WON!");
            gameover = true;
            Console.ReadKey();
        }
    }
}
