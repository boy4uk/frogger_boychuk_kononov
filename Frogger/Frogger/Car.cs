﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Car
    {
        public static string Icon = "*";
        public static Random randomY = new Random();
        public static Random randomX = new Random();
        public int X = randomX.Next(1, Map.Collomns - 2);
        public int Y = randomY.Next(Road.startBorder + 1, Road.endBorder - 1);


        public void Move()
        {
            //первоначальная отрисовка
            Console.SetCursorPosition(X, Y);
            Console.Write(Icon);

            if (X > 1)
            {
                X--;
                Console.SetCursorPosition(X + 1, Y);
                Console.Write(Map.GameMap[Y, X]);
                Console.SetCursorPosition(X, Y);
                Console.Write(Icon);
            }
            else
            {
                Console.SetCursorPosition(X, Y);
                Console.Write(Map.GameMap[Y, X]);
                X = Map.Collomns - 2;
                Random rnd = new Random();
                Y = rnd.Next(Road.startBorder + 1, Road.endBorder);
            }
        }
    }
}
