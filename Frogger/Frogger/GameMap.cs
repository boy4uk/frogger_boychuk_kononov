﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    class Map
    {
        public static int Rows = 20;//высота Y
        public static int Collomns = 41;//ширина X
        public static string[,] GameMap = new string[Rows, Collomns];//Y,X

        public void FillMap()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Collomns; j++)
                    if (i == 0 || i == Rows - 1 || j == 0 || j == Collomns - 1)
                        GameMap[i, j] = "#";
                    else
                        GameMap[i, j] = " ";
            Road.AddRoad();
            Water.AddWater();
        }

        public void DrawMap()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Collomns; j++)
                    Console.Write(GameMap[i, j]);
                Console.WriteLine();
            }
        }
    }

    static class Road
    {
        public static int Length = Map.Collomns;
        public static int startBorder = 10;
        public static int endBorder = 15;

        public static void AddRoad()
        {
            for (int i = startBorder; i <= endBorder; i++)
                for (int j = 1; j < Length - 1; j++)
                    if (i == startBorder || i == endBorder)
                        Map.GameMap[i, j] = "=";
        }
    }
    static class Water
    {
        public static int Length = Map.Collomns;
        public static int startBorder = 3;
        public static int endBorder = 7;

        public static void AddWater()
        {
            for (int i = startBorder; i <= endBorder; i++)
                for (int j = 1; j < Length - 1; j++)
                    Map.GameMap[i, j] = "~";
        }
    }
}
