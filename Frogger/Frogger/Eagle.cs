﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;


namespace Frogger
{
    class Eagle // Это просто мусорка. Все работает, но я даже смотреть сюда не хочу
    {
        public string Icon = "V";
        public int X;
        public int Y = 0;

        public bool startHunt = false;
        public bool eagleAppearedYet = false;

        public static Timer timer = new Timer();

        public Eagle()
        {
            timer.Interval = 3000;
            timer.Elapsed += PlayerIsAFC;
            timer.Start();
        }

        public void PlayerIsAFC(object obj, ElapsedEventArgs e)
        {
            startHunt = true;
        }

        public void Atack()
        {
            if (startHunt)
            {
                if (eagleAppearedYet)
                {
                    X = Frog.X;
                    eagleAppearedYet = false;
                }
                Console.SetCursorPosition(X, Y);
                Console.Write(Map.GameMap[Y, X]);
                if (Y < Map.Rows - 2)
                {
                    Y++;
                    Console.SetCursorPosition(X, Y - 1);
                    Console.Write(Map.GameMap[Y - 1, X]);
                    Console.SetCursorPosition(X, Y);
                    Console.Write(Icon);
                }
                if (Frog.X - X != 0)
                {
                    int direction;
                    if (Frog.X - X < 0)
                        direction = -1;
                    else
                        direction = 1;
                    X = Frog.X;
                    Console.SetCursorPosition(X - direction, Y);
                    Console.Write(Map.GameMap[Y, X - direction]);
                    Console.SetCursorPosition(X, Y);
                    Console.Write(Icon);
                }
                if (X == Frog.X && Y == Frog.Y)
                    Game.GameOver();

            }
        }

        public static void ReserEagleTimer()
        {
            timer.Stop();
            timer.Start();
        }

    }
}
