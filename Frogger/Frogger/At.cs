﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frogger
{
    public class At
    {
        private readonly int _xLimit;
        private readonly int _yLimit;
        public At()
            : this(Console.WindowWidth / 2, 0, Console.WindowWidth - 1, Console.WindowHeight - 2)
        { }

        public At(int x, int y, int xLimit, int yLimit)
        {
            X = x;
            Y = y;
            _xLimit = xLimit;
            _yLimit = yLimit;
            // Производим первичную отрисовку
            Console.SetCursorPosition(X, Y);
            Console.Write('@');
        }
        public int X { get; private set; }
        public int Y { get; private set; }

        public void Move(ConsoleKey direction)
        {
            // Стираем следы (все пиксели игрового объекта на поле)
            Console.SetCursorPosition(X, Y);
            Console.Write(' ');
            switch (direction)
            {
                case ConsoleKey.LeftArrow:
                    if (X > 0) X--;
                    break;
                case ConsoleKey.RightArrow:
                    if (X < _xLimit) X++;
                    break;
                case ConsoleKey.UpArrow:
                    if (Y > 0) Y--;
                    break;
                case ConsoleKey.DownArrow:
                    if (Y < _yLimit)
                        Y++;
                    break;
            }
            // Отрисовываем объект в новой позиции
            Console.SetCursorPosition(X, Y);
            Console.Write('@');
        }
    }
}
